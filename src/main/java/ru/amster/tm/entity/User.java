package ru.amster.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;

@Getter
@Setter
public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String fistName;

    @Nullable
    public String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    @Nullable
    private Boolean Locked = false;

}