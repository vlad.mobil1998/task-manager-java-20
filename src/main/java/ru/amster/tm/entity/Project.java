package ru.amster.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class Project extends AbstractEntity {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String UserId;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}