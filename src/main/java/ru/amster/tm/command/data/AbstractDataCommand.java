package ru.amster.tm.command.data;

import lombok.NoArgsConstructor;
import ru.amster.tm.command.authentication.AbstractAuthCommand;
import ru.amster.tm.dto.Domain;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractAuthCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_XML = "./data.xml";

    protected static final String FILE_JSON = "./data.json";

    protected static final String FILE_BASE64 = "./data.base64";

    public Domain getDomain() {
        Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        return domain;
    }

    public void setDomain(Domain domain) {
        if (domain == null) return;
        serviceLocator.getDomainService().load(domain);
    }

}