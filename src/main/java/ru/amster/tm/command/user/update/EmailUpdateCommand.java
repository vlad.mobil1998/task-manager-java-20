package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class EmailUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-email";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE EMAIL]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        System.out.println("ENTER EMAIL");
        @Nullable final String email = TerminalUtil.nextLine();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateEmail(userId, email);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}