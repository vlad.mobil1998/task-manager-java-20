package ru.amster.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;

public final class UserProfileShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "view-profile";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show you profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROFILE]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new EmptyUserException();

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        if (user.getEmail() != null && user.getEmail().isEmpty()) {
            System.out.println("[EMAIL]");
            System.out.println(user.getEmail());
        }
        if (user.getFistName() != null && user.getFistName().isEmpty()) {
            System.out.println("[FIST NAME]");
            System.out.println(user.getFistName());
        }
        if (user.getMiddleName() != null && user.getMiddleName().isEmpty()) {
            System.out.println("[MIDDLE NAME]");
            System.out.println(user.getMiddleName());
        }
        if (user.getLastName() != null && user.getLastName().isEmpty()) {
            System.out.println("[LAST NAME]");
            System.out.println(user.getLastName());
        }
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}