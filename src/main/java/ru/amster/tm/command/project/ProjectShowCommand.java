package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;

import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final List<Project> projects = projectService.findAll(userId);
        for (@Nullable final Project project : projects) ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}