package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-re-i";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove project by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        System.out.println("ENTER INDEX");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Integer maxIndex = projectService.numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);

        @Nullable final Project project = projectService.removeOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}