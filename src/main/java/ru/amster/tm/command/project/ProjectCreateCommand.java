package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Create new project";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECTS]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();

        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}