package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;
import ru.amster.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-v-i";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task by index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        System.out.println("ENTER INDEX");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Integer maxIndex = taskService.numberOfAllTasks(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);

        @Nullable final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}