package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;

import java.util.List;

public final class TaskShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASK]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final List<Task> tasks = taskService.findAll(userId);
        for (@Nullable final Task task : tasks) TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}