package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-re-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @Nullable final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final Task task = taskService.removeOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}