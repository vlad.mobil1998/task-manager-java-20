package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.exception.user.AccessDeniedException;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-clr";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove all task";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        @NotNull final String userId = authenticationService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}