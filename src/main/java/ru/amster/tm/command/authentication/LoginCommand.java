package ru.amster.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IAuthenticationService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractAuthCommand {

    @Override
    @NotNull
    public String name() {
        return "login";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Sign in system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        if (userActivated.getCheckForActivation()) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();

        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final IAuthenticationService authenticationService = serviceLocator.getAuthService();
        authenticationService.login(login, password);
        userActivated.setCheckForActivation(true);
        System.out.println("[OK]");
    }

}