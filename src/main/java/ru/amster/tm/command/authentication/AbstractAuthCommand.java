package ru.amster.tm.command.authentication;

import lombok.NoArgsConstructor;
import ru.amster.tm.command.AbstractCommand;

@NoArgsConstructor
public abstract class AbstractAuthCommand extends AbstractCommand {

    protected UserActivated userActivated;

    public void setLoginCheck(UserActivated userActivated) {
        this.userActivated = userActivated;
    }

}