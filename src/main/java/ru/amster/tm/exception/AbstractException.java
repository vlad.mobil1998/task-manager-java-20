package ru.amster.tm.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }
}