package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;

public interface IAuthenticationService {

    @NotNull
    String getUserId();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void registration(@NotNull String login, @NotNull String password, @NotNull String email);

    void checkRoles(@Nullable Role[] roles);

}