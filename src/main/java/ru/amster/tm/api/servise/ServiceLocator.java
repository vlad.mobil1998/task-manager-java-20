package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IAuthenticationService getAuthService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

}