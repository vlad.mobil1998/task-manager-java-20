package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : records) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final User user = findById(id);
        if (user == null) throw new EmptyIdException();
        remove(user);
    }

    @NotNull
    @Override
    public void removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        remove(user);
    }

}